# Changes in 0.2.4

* Fix inconsistent internal versions.
* Fix formatting.

# Changes in 0.2.3

* Add features to disable building the assert and abort functions.

# Changes in 0.2.2

* Build system was fixed to re-enable builds from version released via crates.io.

# Changes in 0.2.1

* Using `inmemory_write` messages works is fixed for decryption.
* `ProtectedMessage.into_inner()` is made non-pub.
  While that's technically a breaking change, the function was so broken that all imaginable uses would have gone wrong;
  the high-level API is unaffected.
* Attempts to truncate a read-only message are caught and avoided.
* Tests are extended.

# Changes in 0.2.0

* Use coap-message 0.3 (and coap-message-implementations 0.1.1 with alloc).
* Fix argument mixup between salt and input key material.
* Update tools, reduce build dependencies.

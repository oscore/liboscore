//! Run a full request encryption, decryption, and response en/-decryption cycle.
//!
//! Unlike the `unprotect_{high,low}level`, this doesn't try to be exhaustive in terms of options
//! and payload, and is not based on a plugtest.

use coap_message::{MessageOption, MinimalWritableMessage, ReadableMessage};

use coap_numbers::code::{CONTENT, GET};
use coap_numbers::option::{OSCORE, URI_PATH};

pub fn run() {
    let mut ctx_server = liboscore::PrimitiveContext::new_from_fresh_material(
        liboscore::PrimitiveImmutables::derive(
            liboscore::HkdfAlg::from_number(5).unwrap(),
            b"ikm",
            b"salt",
            None,
            liboscore::AeadAlg::from_number(24).unwrap(),
            b"server",
            b"c",
        )
        .unwrap(),
    );
    let mut ctx_client = liboscore::PrimitiveContext::new_from_fresh_material(
        liboscore::PrimitiveImmutables::derive(
            liboscore::HkdfAlg::from_number(5).unwrap(),
            b"ikm",
            b"salt",
            None,
            liboscore::AeadAlg::from_number(24).unwrap(),
            b"c",
            b"server",
        )
        .unwrap(),
    );

    // Let's have a single place for client and server … CoAP-over-RAM?
    let mut build_code = 0;
    let mut build_buffer = [0; 200];

    // Heap messages are not really fully supported in this role (cf. the FIXME in
    // with_heapmessage_as_msg_native which is used alsi in the WithMsgNative conversion, and also
    // the fact that that constructor doesn't distinguish between new and populated messages, and
    // is geared toward the latter by setting payload_len)
    let mut msg = coap_message_implementations::inmemory_write::Message::new(
        &mut build_code,
        &mut build_buffer,
    );
    let (mut correlate_client, ()) = liboscore::protect_request(&mut msg, &mut ctx_client, |req| {
        req.set_code(GET);
        req.add_option(URI_PATH, b"foo").unwrap();
    })
    .unwrap();

    let length = msg.finish();
    let mut msg = coap_message_implementations::inmemory_write::Message::new_from_existing(
        &mut build_code,
        &mut build_buffer[..length],
    );

    // Sorry, need to copy it out … having a &mut of the message will otherwise be complicated
    let oscopt = msg
        .options()
        .filter(|o| o.number() == OSCORE)
        .next()
        .unwrap()
        .value()
        .to_owned();
    let oscopt = liboscore::OscoreOption::parse(&oscopt).unwrap();
    let (mut correlate_server, ()) =
        liboscore::unprotect_request(&mut msg, oscopt, &mut ctx_server, |req| {
            assert_eq!(req.code(), GET);
            assert_eq!(req.options().next().unwrap().value(), b"foo");
        })
        .unwrap();

    let mut msg = coap_message_implementations::inmemory_write::Message::new(
        &mut build_code,
        &mut build_buffer,
    );

    liboscore::protect_response(&mut msg, &mut ctx_server, &mut correlate_server, |res| {
        res.set_code(CONTENT);
        res.set_payload(b"this is the response").unwrap();
    })
    .unwrap();

    let length = msg.finish();
    let mut msg = coap_message_implementations::inmemory_write::Message::new_from_existing(
        &mut build_code,
        &mut build_buffer[..length],
    );

    let oscopt = msg
        .options()
        .filter(|o| o.number() == OSCORE)
        .next()
        .unwrap()
        .value()
        .to_owned();
    let oscopt = liboscore::OscoreOption::parse(&oscopt).unwrap();
    liboscore::unprotect_response(
        &mut msg,
        &mut ctx_client,
        oscopt,
        &mut correlate_client,
        |res| {
            assert_eq!(res.code(), CONTENT);
            assert_eq!(res.payload(), b"this is the response");
        },
    )
    .unwrap();
}
